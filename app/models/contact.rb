class Contact < ApplicationRecord
    belongs_to :kind
    has_many :phones
    has_one :address

    accepts_nested_attributes_for :phones, allow_destroy: true
    accepts_nested_attributes_for :address, update_only: true #, allow_destroy: true




    #def author
    #    "Grigorio A. Silva"
    #end

    #def as_json(options={})
    #    super(
    #        root: true,
    #        #methods: :author,
    #        include: {kind: {only: :description}}
    #    )
    #end

end
